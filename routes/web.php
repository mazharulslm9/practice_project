<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'WelcomeController@index');
Route::get('add', 'WelcomeController@add');
Route::get('search', 'WelcomeController@search');
Route::get('image', 'WelcomeController@image');
Route::post('resizeImagePost', 'WelcomeController@saveImage');
Route::post('save', 'WelcomeController@save');
Route::any( 'search-result','WelcomeController@searchResult');
Route::post('change-status', 'WelcomeController@changeStatus');


