@extends('welcome')
@section('content')

<div class="container">
        <form action="{{ url('search-result') }}" method="post" role="search">
         @csrf
            <div class="input-group">
                <input type="text" class="form-control" name="q"
                    placeholder="Search users"> <span class="input-group-btn">
                    <button type="submit" class="btn btn-default">
                      Search
                    </button>
                </span>
            </div>
        </form>
            @if(isset($product))
    <h2>Product details</h2>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Action</th>
                    <th>Status</th>
                    <th>Name</th>
                    <th>C Name</th>
                    <th>S Name</th>
                </tr>
            </thead>
                <form action="{{ url('change-status') }}" method="post">
            @csrf
            <tbody>
                @foreach($product as $pp)
                <tr>
                    <td>
                        <input type="checkbox" name="selected[]" value="{{ $pp->product_id }}"></td>
                    <td>{{ $pp->status }}</td>
                    <td>{{$pp->product_name}}</td>
                    <td>{{$pp->category_name}}</td>
                    <td>{{$pp->sub_category_name}}</td>
                </tr>
                @endforeach
 
            </tbody>
        </table>
    
            <input type="submit" name="submit" value="Disable">
            <input type="submit" name="submit" value="Delete">
        </form>
        <br/> <br>
        {!! $product->render() !!}@endif
    </div>

@endsection