<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Common;
use DB;
use Input;
use Image;
use Session;
use Redirect;
class WelcomeController extends Controller
{
    public function index(){
    	 $cat=DB::table('category')->paginate(1);
    	return view('welcome',compact('cat'));
    }
      public function add(){
    	     try {
return view('add');
                }
                catch (\Exception $e) {
                    $err_message = \Lang::get($e->getMessage());
                    return Redirect::to('add')->with('error', $err_message);
                }
    }

    public function save(Request $request){
    	print_r($request->all());
    }
    public function search(){
    	$product = DB::table('products')
            ->join('category', 'products.category_id', '=', 'category.category_id')
            ->join('sub_category', 'products.sub_category_id', '=', 'sub_category.sub_category_id')
            ->select('products.*', 'category.name as category_name', 'sub_category.name as sub_category_name')
            ->paginate(8);
            return view('search',compact('product'));
    }
    public function changeStatus(Request $request){
         // echo "<pre>";
         // print_r($request->all());
         // exit;
        $ids = explode(',',implode(",",$request->selected));

         if ($request->submit=='Disable') {
     DB::table('products')->whereIn('product_id',$ids)
                  ->update(['status' => '1']);
                  return Redirect::to('search');
         }elseif($request->submit=='Delete'){
DB::table('products')->whereIn('product_id',$ids)
                  ->delete();
                  return Redirect::to('search');
         }
    }
    public function searchResult(Request $request){
    $q = $request->input('q');
    if($q != ""){
    	$product= DB::table('products')
            ->join('category', 'products.category_id', '=', 'category.category_id')
            ->join('sub_category', 'products.sub_category_id', '=', 'sub_category.sub_category_id')
            ->where('products.product_name','LIKE', '%' . $q . '%')
            ->orWhere('category.name','LIKE', '%' . $q . '%')
            ->orWhere('sub_category.name','LIKE', '%' . $q . '%')
            ->select('products.*', 'category.name as category_name', 'sub_category.name as sub_category_name')
            ->paginate(2)->setPath ( '' );
    $pagination = $product->appends ( array (
                'q' =>$request->input('q')
        ) );
    if (count ( $product ) > 0)
        return view ('search',compact('product','q'));
    }
        return view ('search')->withMessage ('No Details found. Try to search again !' );
    }
    public function image (){
    	return view('image');
    }
    public function saveImage(Request $request){
    		    $this->validate($request, [
	    	'title' => 'required',
	    	'image' => 'required',
            'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5048',
        ]);
if($request->hasFile('image'))
{
    $files = $request->file('image');
    foreach ($files as $file) {
        $filename = time().'.'.$file->getClientOriginalExtension();
        $file->move(public_path().'/'.'images', $filename);
    }
}

// waterwark and resize
  //       $image = $request->file('image');
  //       $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
   

  //       $destinationPath = public_path('thumbnail');
  //       $img = Image::make($image->getRealPath());
  //           $img->insert(public_path('logo/logo.png'), 'bottom-right', 10, 10);
  //   $img->save(public_path('images/'. $input['imagename'])); 
  //       $img->resize(100, 100, function ($constraint) {
		//     $constraint->aspectRatio();
		// })->save($destinationPath.$input['imagename']);


// nes
        // $destinationPath = public_path('images');
        // $image->move($destinationPath, $input['imagename']);


        // $data['name']=$request->title;
        // $data['url']=$input['imagename'];
        // DB::table('images')->insert($data);


        return back()
        	->with('success','Image Upload successful');
        	// ->with('imageName',$input['imagename']);
    }
    
}
